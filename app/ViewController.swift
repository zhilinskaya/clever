//
//  ViewController.swift
//  app
//
//  Created by Anastasia Zhilinskaya on 6.02.22.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    let langField = UITextField(frame: CGRect(x: UIScreen.main.bounds.size.width*0.5, y: UIScreen.main.bounds.size.height*0.5, width: 180, height: 30))
   var dayButton = UIButton()
    var nightButton = UIButton()
    var autoButton = UIButton()
    let dayImage = UIImage(named: "day")
    let dayImage_white = UIImage (named: "day_white")
    let nightImage = UIImage(named: "night")
    let nightImage_white = UIImage (named: "night_white")
    let autoImage = UIImage(named: "auto")
    let autoImage_white = UIImage(named: "auto_white")
    let array = ["English","Русский","Белорусский"]
    var firstLabel = UILabel()
    let calendar = Calendar.current
    let now = Date()

var pickerView = UIPickerView()
    override func viewDidLoad() {
        view.backgroundColor = .white
        let imageView = UIImageView (frame: CGRect ( x: 40, y: 40 , width: self.view.frame.size.width/4 , height: self.view.frame.size.height / 7 ))
        imageView.image = UIImage(named: "app.logo")
        self.view.addSubview(imageView)
        
        let labelFrame = CGRect (x: 120, y: 500, width: 375, height: 40)
              firstLabel.frame = labelFrame

              firstLabel.font = UIFont.boldSystemFont(ofSize: 30)
              firstLabel.center = self.view.center
              view.addSubview(firstLabel)
   
        firstLabel.text = NSLocalizedString("Hello", comment: "")
      
        dayButton = UIButton(type: .roundedRect)
        dayButton.frame = CGRect (x: 30, y: 200, width: 40, height: 44)
        dayButton.setTitle("d", for: .normal)
        dayButton.setTitle("day", for: .highlighted)
        dayButton.addTarget(self, action: #selector(LightTheme), for: .touchDown )
        dayButton.tintColor = .white
        dayButton.setBackgroundImage(dayImage, for: .normal)
        self.view.addSubview(dayButton)
        
        nightButton = UIButton(type: .roundedRect)
        nightButton.frame = CGRect (x: 80, y: 200, width: 40, height: 44)
        nightButton.setTitle("n", for: .normal)
        nightButton.setTitle("night", for: .highlighted)
        nightButton.addTarget(self, action: #selector(NightTheme), for: .touchDown )
        nightButton.setBackgroundImage(nightImage, for: .normal)
        nightButton.tintColor = .brown
        self.view.addSubview(nightButton)
        
        autoButton = UIButton(type: .roundedRect)
        autoButton.frame = CGRect (x: 130, y: 200, width: 40, height: 44)
        autoButton.setTitle("a", for: .normal)
        autoButton.setTitle("auto", for: .highlighted)
        autoButton.addTarget(self, action: #selector(AutoTheme), for: .touchDown )
        autoButton.setBackgroundImage(autoImage, for: .normal)
        autoButton.tintColor = .gray
        
      
        
        self.view.addSubview(autoButton)
        
        super.viewDidLoad()
        displayLoginFields()
        langField.inputView = pickerView
        langField.textAlignment = .center
      
        pickerView.delegate = self
        pickerView.dataSource = self
       
        self.view.addSubview(langField)
    

   

    }
    
   
    @objc func buttonIsPressed (sender : UIButton){
        print("Button is pressed")
    }
    @objc func buttonIsTapped (sender : UIButton){
        print("Button is tapped")
    }
   
    
    func displayLoginFields(){
        langField.placeholder = "select language"
        langField.backgroundColor = .white

    }
    
    @objc func NightTheme()
    {
        self.view.backgroundColor = .black
        self.firstLabel.textColor = .white
        let whiteimage = UIImageView (frame: CGRect ( x: 40, y: 40 , width: self.view.frame.size.width/4 , height: self.view.frame.size.height / 7 ))
        whiteimage.image = UIImage(named: "app.white.logo")
        self.view.addSubview(whiteimage)
        self.dayButton.setBackgroundImage(dayImage_white, for: .normal)
        self.dayButton.tintColor = .black
        self.nightButton.setBackgroundImage(nightImage_white, for: .normal)
        self.autoButton.setBackgroundImage(autoImage_white, for: .normal)
    }
    
    @objc func LightTheme()
    {
        self.view.backgroundColor = .white
        self.firstLabel.textColor = .black
        let blackimage = UIImageView (frame: CGRect ( x: 40, y: 40 , width: self.view.frame.size.width/4 , height: self.view.frame.size.height / 7 ))
        blackimage.image = UIImage(named: "app.logo")
        self.view.addSubview(blackimage)
        self.dayButton.setBackgroundImage(dayImage, for: .normal)
        self.dayButton.tintColor = .white
        self.nightButton.setBackgroundImage(nightImage, for: .normal)
        self.autoButton.setBackgroundImage(autoImage, for: .normal)
    }
    @objc func AutoTheme()
    {
        let nightstart = calendar.date(
          bySettingHour: 22,
          minute: 00,
          second: 00,
          of: now)!
        let nightend = calendar.date(
          bySettingHour: 08,
          minute: 00,
          second: 00,
          of: now)!
        if now >= nightstart && now <= nightend {
            NightTheme()
        } else {
            LightTheme()
        }
    }
 
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
     return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return array.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return array[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch row {
        case 0:
            self.firstLabel.text = "Hello"
        case 1:
            self.firstLabel.text = "Привет"
        case 2:
            self.firstLabel.text = "Добры дзень"
        default :
            break
        }
        langField.text = array[row]
        langField.resignFirstResponder()
        }
    
}





